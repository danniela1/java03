package deloitte.Academy.lesson01.operation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import deoitte.Academy.lesson01.enums.Categoria;

public class Producto {

	private static final Logger LOGGER = Logger.getLogger(Producto.class.getName());

	public String nombre;
	public Categoria categoria;
	public int stock;
	public int ctd;


	// constructor

	public Producto(String nombre,  int stock, int ctd) {
		super();
		this.nombre = nombre;
		this.stock = stock;
		this.ctd = ctd;

	}

	// gets & sets

	public Producto() {
		// TODO Auto-generated constructor stub
	}

	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getCtd() {
		return ctd;
	}

	public void setCtd(int ctd) {
		this.ctd = ctd;
	}

	/**
	 * metodo que se encarga de agregar productos a la lista.
	 * 
	 * @param productos
	 * @param producto
	 * @return
	 */
	public static ArrayList<Producto> addProduct(ArrayList<Producto> productos, Producto producto) {
		ArrayList<Producto> claseProducts = new ArrayList<Producto>();
		claseProducts = productos;
				
		claseProducts.add(producto);
		return claseProducts;
	
	}

	
	// Comprar producto 
	public static ArrayList<Producto> vendido(ArrayList<Producto> productos, Producto producto) {
		ArrayList<Producto> claseProductos = new ArrayList<Producto>();
		ArrayList<OperacionesProducto> listVendidos = new ArrayList<OperacionesProducto>();		
		int almacen = 20;
		claseProductos = productos;

		for (Producto reg : productos) {

			if (reg.equals(producto)) {
				
				claseProductos.remove(producto);
								
				almacen--;
				
				break;
			}else { 
				if(almacen == 0) { LOGGER.info("No hay en existencia"); } }
		}
		LOGGER.info("Producto vendido, queda en almacen: " + almacen);
		return claseProductos;
	}


	// metodo para ordenar por categoria
	/*public ArrayList<Producto> ordenar(ArrayList<Producto> productos, Producto producto) {

		ArrayList<Producto> claseProductos = new ArrayList<Producto>();
		claseProductos = productos;
		for (Producto reg : productos) {
			if (reg.categoria == "lacteos") {
				LOGGER.info("Categoria encontrada: " + reg.categoria);
				break;
			} else {
				LOGGER.info("no hay categorias similares");
			}
		}
		return null;
	}*/

}
