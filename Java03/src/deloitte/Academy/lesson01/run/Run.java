package deloitte.Academy.lesson01.run;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import deloitte.Academy.lesson01.operation.OperacionesProducto;
import deloitte.Academy.lesson01.operation.Producto;
import deoitte.Academy.lesson01.enums.Categoria;
import deoitte.Academy.lesson01.enums.Mensajes;

public class Run {

	private static final Logger LOGGER = Logger.getLogger(Producto.class.getName());
	
	public static final List<Producto> listaProductos = new ArrayList<Producto>();
	public static final List<Producto> listaProductosVendidos = new ArrayList<Producto>();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Producto producto1 = new Producto();
		Producto producto2 = new Producto();
		Producto productoNuevo = new Producto();
		
		producto1.setStock(1);
		producto1.setNombre("lup");
		producto1.setCtd(21);
		
		producto1.setStock(2);
		producto1.setNombre("zap");
		producto1.setCtd(33);
		
		producto1.setStock(5);
		producto1.setNombre("iuj");
		producto1.setCtd(98);
		
		productoNuevo.setStock(66);
		productoNuevo.setNombre("asdf");
		productoNuevo.setCtd(87);
		
		listaProductos.add(producto1);
		listaProductos.add(producto2);
		
		OperacionesProducto opProd = new OperacionesProducto();
		
		opProd.agregarProducto(productoNuevo);
		
		
		for(Producto prod: listaProductos) {
			System.out.println("stock" + prod.getStock() + "nombre: "+ prod.getNombre() + "cantidad: " +prod.getCtd());
		}
		
		opProd.venderProducto(producto2, 5);
		
		
		
		
		
		
		//agregado de producto
		Producto p1 = new Producto();
		p1.setNombre("zapato");
		p1.setStock(10);
		p1.setCategoria(Categoria.Ropa);
		
		Producto p2 = new Producto();
		p2.setNombre("leche");
		p2.setStock(10);
		p2.setCategoria(Categoria.Electrónica);

		Producto p3 = new Producto();
		p3.setNombre("cremaa");
		p3.setStock(10);
		p3.setCategoria(Categoria.Ropa);

		
		ArrayList<Producto> productoRegistro = new ArrayList<Producto>();
		productoRegistro.add(p1);
		productoRegistro.add(p2);
		productoRegistro.add(p3);
		
		//Agregar producto nuevo
		ArrayList<Producto> objetoResul = new ArrayList<Producto>();
		objetoResul = Producto.addProduct(productoRegistro, p1);
		System.out.println(Mensajes.OK.getMensaje()); 
		
		
		//muestra categoria similar
		//Producto pro = new Producto();
	//	pro.ordenar(productoRegistro, null);
		
		//elimina producto
		objetoResul = Producto.vendido(productoRegistro, p2);
		System.out.println(Mensajes.Vendido.getMensaje());
		
		
		
	}

}
