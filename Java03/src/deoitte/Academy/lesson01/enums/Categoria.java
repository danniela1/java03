package deoitte.Academy.lesson01.enums;

/**
 * muestra las categorias disponibles en la tienda en linea
 * @author nnavarrete
 *
 */
public enum Categoria {

	Electrónica, Ropa;	
	
	
}
