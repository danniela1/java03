package deoitte.Academy.lesson01.enums;

public enum Mensajes {
	
	OK("Producto agregado con exito."), Error("Error en registro."), Eliminado("Eliminado con exito"), Vendido("Compra realizada");
	
	public String mensaje;

	//constructor
	private Mensajes(String mensaje) {
		this.mensaje = mensaje;
	}

	
	//gets & sets
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
	
	
	
	

}
